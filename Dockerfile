FROM microsoft/dotnet:2.2-sdk

RUN apt update && apt install -y default-jdk && \
    dotnet tool install --global dotnet-sonarscanner
